/*
  config.h
  
  Definitions in here are pretty safe to modify. Generally user defined
  stuff anyway ;)
  
*/

/* Location of the various external files */
#define FILE_MOTD    "game.motd"	/* Message of the Day File */
#define FILE_PMOTD    "game.pmotd"	/* Playback motd */
#define FILE_CONF    "game.conf"	/* Game configuration File */
#define FILE_WINLIST "game.winlist"	/* Winlist storage file */
#define FILE_WINLIST2 "game.winlist2"	/* Winlist storage file */
#define FILE_WINLIST3 "game.winlist3"	/* Winlist storage file */

#define FILE_BAN     "game.ban"		/* List of Banned IP's */
#define FILE_BAN_COMPROMISE     "game.ban.compromise"	/* List of Banned IP's */
#define FILE_ALLOW   "game.allow"	/* List of allow IP's */
#define FILE_LOG     "game.log"		/* Logfile */
#define FILE_PID     "game.pid"		/* Default PID */
#define FILE_SECURE  "game.secure"	/* Security file */
