/*
  game.h
  
  Contains any configuration type external file functions.
  This includes all functions to read and write the game configuration
  files, and of course the winlist functions.
  
*/

/* Write, in plain english, with comments, a game.conf file from current settings */
int gamewrite(void);

/* Read, from a text game.conf file, game settings */
int gameread(void);

/* Initialise game settings to default values */
void init_game(void);

/* Updates a current entry, or creates a new entry with score */
void updatewinlist(char *name, char status, int score, struct net_t *n);

/* For special game types onetetris and timelimit */
void update_ntetris_winlist(struct net_t *n);
void update_timelimit_winlist(struct net_t *n);
void update_survival_winlist(struct net_t *n);
void update_pwdlist();

struct winlist_t *find_winlist(struct net_t *n);
struct winlist_t *find_winlist_from_chan(char *chan_name);
void find_winlist_file(struct net_t *n, char *fn);
int find_winlist_size(struct net_t *n);

/* Initialise Winlist structure */
void init_winlist(struct winlist_t *w, int max_entries);
void init_pwdlist();

/* Read Winlist, from winlist file */
void readwinlist(char *fname, struct winlist_t *w, int max_entries);
void read_pwdlist();

/* Write Winlist to winlist file*/
void writewinlist(struct net_t *n);
void write_pwdlist();

/* Send Winlist to players */
void sendwinlist(struct net_t *n);
void sendwinlist_to_all(struct net_t *n);

