// CONFIG
#define FILE_MOTD		"/etc/tetrinetx/game.motd"
#define FILE_PMOTD		"/etc/tetrinetx/game.pmotd"
#define FILE_CONF		"/etc/tetrinetx/game.conf"
#define FILE_BAN		"/etc/tetrinetx/game.ban"
#define FILE_ALLOW		"/etc/tetrinetx/game.allow"
#define FILE_SECURE		"/etc/tetrinetx/game.secure"
#define FILE_BAN_COMPROMISE	"/etc/tetrinetx/game.ban.compromise"

// LOG & PID
#define FILE_LOG		"/var/log/tetrinetx/game.log"
#define FILE_PID		"/var/run/tetrinetx/game.pid"

// SCORES
#define FILE_WINLIST		"/var/games/tetrinetx/game.winlist"
#define FILE_WINLIST2		"/var/games/tetrinetx/game.winlist2"
#define FILE_WINLIST3		"/var/games/tetrinetx/game.winlist3"
